#include "compositeshape.hpp"
#include <iostream>
#include <stdexcept>
#include <memory>
#include <cstdlib>
#include <algorithm>

CompositeShape::CompositeShape(Shape* shape) :
  array_(new Shape*[1]),
  arr_size_(1)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Object is null");
  }
  array_[0] = shape;
}
Shape& CompositeShape::operator[](size_t n) const
{
  if (n >= arr_size_)
  {
    throw std::invalid_argument("Out of array range");
  }
  return *(array_[n]);
}
bool operator==(const CompositeShape& cs_lhs, const CompositeShape& cs_rhs)
{
  if (cs_lhs.arr_size_ != cs_rhs.arr_size_)
  {
    throw std::invalid_argument("Arrays has a different sizes");
  }
  for (size_t i = (cs_lhs.arr_size_ - 1); i > 0; i--)
  {
    if (cs_lhs.array_[i] == cs_rhs.array_[i])
    {
      return true;
    }
  }
  return false;
}
double CompositeShape::getArea() const
{
  double area = array_[0]->getArea();
  for (size_t i = 1; i < arr_size_; i++)
  {
    area += array_[i]->getArea();
  }
  return area;
}
rectangle_t CompositeShape::getFrameRect() const
{
 int dkey = 2;
  rectangle_t rect = array_[0]->getFrameRect();
  double minX = rect.pos.x - rect.width / dkey;
  double maxX = rect.pos.x + rect.width / dkey;
  double minY = rect.pos.y - rect.height / dkey;
  double maxY = rect.pos.y + rect.height / dkey;
  for (size_t i = 1; i < arr_size_; i++)
  {
    rect = array_[i]->getFrameRect();
    minX = std::min(minX, rect.pos.x - rect.width / 2);
    maxX = std::max(maxX, rect.pos.x + rect.width / 2);
    minY = std::min(minY, rect.pos.y - rect.height / 2);
    maxY = std::max(maxY, rect.pos.y + rect.height / 2);
  }
  return { maxX - minX, maxY - minY, { maxX - (maxX - minX) / 2, maxY - (maxY - minY) / 2 } };
}
void CompositeShape::move(const point_t &to_position)
{
  point_t tmp_centre = getFrameRect().pos;
  for (size_t i = 0; i < arr_size_; i++)
  {
    array_[i]->move(tmp_centre.x - to_position.x, tmp_centre.y - to_position.y);
  }
}
void CompositeShape::move(const double &dx, const double &dy)
{
  for (size_t i = 0; i < arr_size_; i++)
  {
    array_[i]->move(dx, dy);
  }
}
void CompositeShape::scale(const double &k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("k <= 0");
  }
  point_t tmp_centre = getFrameRect().pos;
  for (size_t i = 0; i < arr_size_; i++)
  {
    array_[i]->move({ tmp_centre.x + (array_[i]->getFrameRect().pos.x - tmp_centre.x) * k,
     tmp_centre.y + (array_[0]->getFrameRect().pos.y - tmp_centre.y) * k });
    array_[i]->scale(k);
  }
}
void CompositeShape::printShapeInfo() const
{
  std::cout << "CompositeShape shapes info" << "\n";
  for (size_t i = 0; i < arr_size_; i++)
  {
    array_[i]->printShapeInfo();
  }
}
void CompositeShape::addShape(Shape* shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Object is null");
  }
  std::unique_ptr<Shape*[]> temp_array(new Shape*[arr_size_ + 1]);
  for (size_t i = 0; i < arr_size_; i++)
  {
    temp_array[i] = array_[i];
  }
  temp_array[arr_size_++] = shape;
  array_.swap(temp_array);
}
size_t CompositeShape::getSize() const
{
  return arr_size_;
}