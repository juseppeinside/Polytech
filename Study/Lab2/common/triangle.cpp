#include "triangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

Triangle::Triangle(const point_t &A, const point_t &B, const point_t &C) :
  pos_({ (A.x + B.x + C.x) / 3 , (A.y + B.y + C.y) / 3 }),
  pointA_(A),
  pointB_(B),
  pointC_(C)
{
  if (getArea() == 0)
  {
    throw std::invalid_argument("Point of triange must be different!");
  }
}
double Triangle::getAC() const
{
  double AC_x = pow(pointC_.x - pointA_.x, 2);
  double AC_y = pow(pointC_.y - pointA_.y, 2);
  return (sqrt(AC_x + AC_y));
}
double Triangle::getAB() const
{
  double AB_x = pow(pointB_.x - pointA_.x, 2);
  double AB_y = pow(pointB_.y - pointA_.y, 2);
  return (sqrt(AB_x + AB_y));
}
double Triangle::getBC() const
{
  double BC_x = pow(pointC_.x - pointB_.x, 2);
  double BC_y = pow(pointC_.y - pointB_.y, 2);
  return (sqrt(BC_x + BC_y));
}
void Triangle::setPoints(const point_t &A, const point_t &B, const point_t &C)
{
  pointA_ = A;
  pointB_ = B;
  pointC_ = C;
}
double Triangle::getMin(const double &A, const double &B, const double &C)
{
  double min = A;
  if (B < min)
  {
    min = B;
  }
  if (C < min)
  {
    min = C;
  }
  return min;
}
double Triangle::getMax(const double &A, const double &B, const double &C)
{
  double max = A;
  if (B > max)
  {
    max = B;
  }
  if (C > max)
  {
    max = C;
  }
  return max;
}
double Triangle::getArea() const
{
  return ((std::abs(((pointA_.x - pointC_.x) * (pointB_.y - pointC_.y)) - ((pointA_.y - pointC_.y) * (pointB_.x - pointC_.x))) / 2));
}
rectangle_t Triangle::getFrameRect() const
{
  double max_x = getMax(pointA_.x, pointB_.x, pointC_.x);
  double min_x = getMin(pointA_.x, pointB_.x, pointC_.x);
  double max_y = getMax(pointA_.y, pointB_.y, pointC_.y);
  double min_y = getMin(pointA_.y, pointB_.y, pointC_.y);
  return rectangle_t{ max_x - min_x, max_y - min_y, pos_ };
}
void Triangle::move(const point_t &to_position)
{
  pointA_.x += to_position.x - pos_.x;
  pointB_.x += to_position.x - pos_.x;
  pointC_.x += to_position.x - pos_.x;
  pointA_.y += to_position.y - pos_.y;
  pointB_.y += to_position.y - pos_.y;
  pointC_.y += to_position.y - pos_.y;
  pos_ = to_position;
}
void Triangle::move(const double &dx, const double &dy)
{
  pos_.x += dx;
  pointA_.x += dx;
  pointB_.x += dx;
  pointC_.x += dx;
  pos_.y += dy;
  pointA_.y += dy;
  pointB_.y += dy;
  pointC_.y += dy;
}
void Triangle::scale(const double &k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("k <= 0!");
  }
  pointA_ = { pos_.x + (pointA_.x - pos_.x) * k, pos_.y + (pointA_.y - pos_.y) * k };
  pointB_ = { pos_.x + (pointB_.x - pos_.x) * k, pos_.y + (pointB_.y - pos_.y) * k };
  pointC_ = { pos_.x + (pointC_.x - pos_.x) * k, pos_.y + (pointC_.y - pos_.y) * k };
}
void Triangle::printShapeInfo() const
{
  std::cout << "Triangle info" << "\n";
  std::cout << "Centre is in " << " X= " << pos_.x << " Y= " << pos_.y << "\n";
  std::cout << "A.x= " << pointA_.x << " A.y= " << pointA_.y << "\n";
  std::cout << "B.x= " << pointB_.x << " B.y= " << pointB_.y << "\n";
  std::cout << "C.x= " << pointC_.x << " C.y= " << pointC_.y << "\n";
}
