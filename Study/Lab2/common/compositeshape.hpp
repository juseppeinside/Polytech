#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include "shape.hpp"
#include <memory>

class CompositeShape : public Shape
{
public:
  CompositeShape(Shape* shape);
  CompositeShape(const CompositeShape &compositeshape) = delete;
  Shape& operator[](size_t n) const;
  friend bool operator==(const CompositeShape& cs_lhs, const CompositeShape& cs_rhs);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &to_position) override;
  void move(const double &dx, const double &dy) override;
  void scale(const double &k) override;
  void printShapeInfo() const override;
  void addShape(Shape* shape);
  void addShape(const CompositeShape &compositeshape) = delete;
  size_t getSize() const;
private:
  std::unique_ptr<Shape*[]> array_;
  size_t arr_size_;
};

#endif
