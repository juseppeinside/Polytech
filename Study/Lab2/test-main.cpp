#ifndef BOOST_TEST_MAIN
#define BOOST_TEST_MAIN

#include "Rectangle.hpp"
#include "Circle.hpp"
#include "Triangle.hpp"
#include <boost/test/included/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

const double k = 0.001;

BOOST_AUTO_TEST_SUITE(Rectangle_tests)
BOOST_AUTO_TEST_CASE(width_on_k)
{
	Rectangle oRectangle{ { 1, 1 }, 3, 4 };
	const double width = oRectangle.getWidth();
	oRectangle.move(2, 3);
	BOOST_CHECK_CLOSE(oRectangle.getWidth(), width, k);
} 
BOOST_AUTO_TEST_CASE(width_to_k)
{
	Rectangle oRectangle{ { 1, 1 }, 3, 4 };
	const double width = oRectangle.getWidth();
	oRectangle.move({ 5, 5 });
	BOOST_CHECK_CLOSE(oRectangle.getWidth(), width, k);
}
BOOST_AUTO_TEST_CASE(height_on)
{
	Rectangle oRectangle{ { 1, 1 }, 3, 4 };
	const double height = oRectangle.getHeight();
	oRectangle.move(2, 3);
	BOOST_CHECK_CLOSE(oRectangle.getHeight(), height, k);
}
BOOST_AUTO_TEST_CASE(height_to)
{
	Rectangle oRectangle{ { 1, 1 }, 3, 4 };
	const double height = oRectangle.getHeight();
	oRectangle.move({ 5, 5 });
	BOOST_CHECK_CLOSE(oRectangle.getHeight(), height, k);
}
BOOST_AUTO_TEST_CASE(area_move_ogn)
{
	Rectangle oRectangle{ { 1, 1 }, 3, 4 };
	const double area = oRectangle.getArea();
	oRectangle.move(2, 3);
	BOOST_CHECK_CLOSE(oRectangle.getArea(), area, k);
}
BOOST_AUTO_TEST_CASE(area_move_tog)
{
	Rectangle oRectangle{ { 1, 1 }, 3, 4 };
	const double area = oRectangle.getArea();
	oRectangle.move({ 5, 5 });
	BOOST_CHECK_CLOSE(oRectangle.getArea(), area, k);
}
BOOST_AUTO_TEST_CASE(area_scaling_plus_rect)
{
	Rectangle oRectangle{ { 1, 1 }, 3, 4 };
	double area = oRectangle.getArea();
	oRectangle.scale(2);
	BOOST_CHECK_CLOSE(oRectangle.getArea(), 4 * area, k);
}
BOOST_AUTO_TEST_CASE(area_scaling_minus_rect)
{
	Rectangle oRectangle{ { 1, 1 }, 3, 4};
	double area = oRectangle.getArea();
	oRectangle.scale(0.5);
	BOOST_CHECK_CLOSE(oRectangle.getArea(), area / 4, k);
}
BOOST_AUTO_TEST_CASE(width_test)
{
	Rectangle oRectangle{ { 1, 1 }, 3, 4 };
	BOOST_CHECK_THROW(oRectangle.setWidth(0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(height_test)
{
	Rectangle oRectangle{ { 1, 1 }, 3, 4 };
	BOOST_CHECK_THROW(oRectangle.setHeight(0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scale_test)
{
	Rectangle oRectangle{ { 1, 1 }, 3, 4 };
	BOOST_CHECK_THROW(oRectangle.scale(0), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()





BOOST_AUTO_TEST_SUITE(Circle_tests)

BOOST_AUTO_TEST_CASE(radius_move_on_k) {
	Circle oCircle{ { 1, 1 }, 4 };
	const double radius = oCircle.getRadius();
	oCircle.move(2, 3);
	BOOST_CHECK_EQUAL(oCircle.getRadius(), radius);
}

BOOST_AUTO_TEST_CASE(radius_move_to_k) {
	Circle oCircle{ { 1, 1 }, 4 };
	const double radius = oCircle.getRadius();
	oCircle.move({ 5, 5 });
	BOOST_CHECK_EQUAL(oCircle.getRadius(), radius);
}
BOOST_AUTO_TEST_CASE(area_move_ong) {
  Circle oCircle{ { 1, 1 }, 4 };
	const double area = oCircle.getArea();
	oCircle.move(2, 3);
	BOOST_CHECK_EQUAL(oCircle.getArea(), area);
}
BOOST_AUTO_TEST_CASE(area_move_tov) {
	Circle oCircle{ { 1, 1 }, 4 };
	const double area = oCircle.getArea();
	oCircle.move({ 5, 5 });
	BOOST_CHECK_EQUAL(oCircle.getArea(), area);
}
BOOST_AUTO_TEST_CASE(area_plus) {
	Circle oCircle{ { 1, 1 }, 4 };
	double area = oCircle.getArea();
	oCircle.scale(2);
	BOOST_CHECK_EQUAL(oCircle.getArea(), 4 * area);
}
BOOST_AUTO_TEST_CASE(area_minus) {
	Circle oCircle{ { 1, 1 }, 4 };
	double area = oCircle.getArea();
	oCircle.scale(0.5);
	BOOST_CHECK_EQUAL(oCircle.getArea(), area / 4);
}
BOOST_AUTO_TEST_CASE(radius_test)
{
	Circle oCircle{ { 1, 1 }, 4 };
	BOOST_CHECK_THROW(oCircle.setRadius(0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scale_test)
{
	Circle oCircle{ { 1, 1 }, 4 };
	BOOST_CHECK_THROW(oCircle.scale(0), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(Triangle_tests)
BOOST_AUTO_TEST_CASE(sides_on_tr)
{
	point_t pointA_t{ 0, 0 }, pointB_t{ 10, 10 }, pointC_t{ 20, 0 };
	point_t centre_t = { (pointA_t.x + pointB_t.x + pointC_t.x) / 3 , (pointA_t.y + pointB_t.y + pointC_t.y) / 3 };
	Triangle oTriangle{ centre_t, pointA_t , pointB_t, pointC_t };
	const double AC = oTriangle.getAC();
	const double AB = oTriangle.getAB();
	const double BC = oTriangle.getBC();
	oTriangle.move(2, 3);
	BOOST_CHECK_CLOSE(oTriangle.getAC(), AC, k);
	BOOST_CHECK_CLOSE(oTriangle.getAB(), AB, k);
	BOOST_CHECK_CLOSE(oTriangle.getBC(), BC, k);
}
BOOST_AUTO_TEST_CASE(sides_to_tr)
{
	point_t pointA_t{ 0, 0 }, pointB_t{ 10, 10 }, pointC_t{ 20, 0 };
	point_t centre_t = { (pointA_t.x + pointB_t.x + pointC_t.x) / 3 , (pointA_t.y + pointB_t.y + pointC_t.y) / 3 };
	Triangle oTriangle{ centre_t, pointA_t , pointB_t, pointC_t };
	const double AC = oTriangle.getAC();
	const double AB = oTriangle.getAB();
	const double BC = oTriangle.getBC();
	oTriangle.move({ 5, 5 });
	BOOST_CHECK_CLOSE(oTriangle.getAC(), AC, k);
	BOOST_CHECK_CLOSE(oTriangle.getAB(), AB, k);
	BOOST_CHECK_CLOSE(oTriangle.getBC(), BC, k);
}
BOOST_AUTO_TEST_CASE(area_on_tr)
{
	point_t pointA_t{ 0, 0 }, pointB_t{ 10, 10 }, pointC_t{ 20, 0 };
	point_t centre_t = { (pointA_t.x + pointB_t.x + pointC_t.x) / 3 , (pointA_t.y + pointB_t.y + pointC_t.y) / 3 };
	Triangle oTriangle{ centre_t, pointA_t , pointB_t, pointC_t };
	const double area = oTriangle.getArea();
	oTriangle.move(2, 3);
	BOOST_CHECK_CLOSE(oTriangle.getArea(), area, k);
}
BOOST_AUTO_TEST_CASE(area_to_tr)
{
	point_t pointA_t{ 0, 0 }, pointB_t{ 10, 10 }, pointC_t{ 20, 0 };
	point_t centre_t = { (pointA_t.x + pointB_t.x + pointC_t.x) / 3 , (pointA_t.y + pointB_t.y + pointC_t.y) / 3 };
	Triangle oTriangle{ centre_t, pointA_t , pointB_t, pointC_t };
	const double area = oTriangle.getArea();
	oTriangle.move({ 5, 5 });
	BOOST_CHECK_CLOSE(oTriangle.getArea(), area, k);
}
BOOST_AUTO_TEST_CASE(area_scaling_plus_tri)
{
	point_t pointA_t{ 0, 0 }, pointB_t{ 10, 10 }, pointC_t{ 20, 0 };
	point_t centre_t = { (pointA_t.x + pointB_t.x + pointC_t.x) / 3 , (pointA_t.y + pointB_t.y + pointC_t.y) / 3 };
	Triangle oTriangle{ centre_t, pointA_t , pointB_t, pointC_t };
	double area = oTriangle.getArea();
	oTriangle.scale(2);
	BOOST_CHECK_CLOSE(oTriangle.getArea(), 4 * area, k);
}
BOOST_AUTO_TEST_CASE(area_scaling_minus_tri)
{
	point_t pointA_t{ 0, 0 }, pointB_t{ 10, 10 }, pointC_t{ 20, 0 };
	point_t centre_t = { (pointA_t.x + pointB_t.x + pointC_t.x) / 3 , (pointA_t.y + pointB_t.y + pointC_t.y) / 3 };
	Triangle oTriangle{ centre_t, pointA_t , pointB_t, pointC_t };
	double area = oTriangle.getArea();
	oTriangle.scale(0.5);
	BOOST_CHECK_CLOSE(oTriangle.getArea(), area / 4, k);
}
BOOST_AUTO_TEST_CASE(scale_test)
{
	point_t pointA_t{ 0, 0 }, pointB_t{ 10, 10 }, pointC_t{ 20, 0 };
	point_t centre_t = { (pointA_t.x + pointB_t.x + pointC_t.x) / 3 , (pointA_t.y + pointB_t.y + pointC_t.y) / 3 };
	Triangle oTriangle{ centre_t, pointA_t , pointB_t, pointC_t };
	BOOST_CHECK_THROW(oTriangle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

#endif
