#include <iostream>
#include "Circle.hpp"
#include "Rectangle.hpp"
#include "Triangle.hpp"

int main()
{
	Rectangle oRectangle{ { 1, 1 }, 2, 3 };
	std::cout << "Rectangle: " << "\n";
	oRectangle.printShapeInfo();
	std::cout << "" << "\n";
	std::cout << "S= " << oRectangle.getArea() << "\n";
	std::cout << "" << "\n";
	oRectangle.move(2, 3);
	oRectangle.printShapeInfo();
	std::cout << "" << "\n";
	oRectangle.move({ 5, 5 });
	oRectangle.printShapeInfo();
	oRectangle.getFrameRect();
	std::cout << "\n";

	Circle oCircle{ { 1, 1 }, 6 };
	std::cout << "Circle: " << "\n";
	oCircle.printShapeInfo();
	std::cout << "" << "\n";
	std::cout << "S= " << oCircle.getArea() << "\n";
	std::cout << "" << "\n";
	oCircle.move(2, 3);
	oCircle.printShapeInfo();
	std::cout << "" << "\n";
	oCircle.move({ 5, 5 });
	oCircle.printShapeInfo();
	std::cout << "\n";

	point_t pointA_t{ 0, 0 }, pointB_t{ 10, 10 }, pointC_t{ 20, 0 };
	point_t centre_t = { (pointA_t.x + pointB_t.x + pointC_t.x) / 3 , (pointA_t.y + pointB_t.y + pointC_t.y) / 3 };
	Triangle oTriangle{ centre_t, pointA_t , pointB_t, pointC_t };
	std::cout << "Triangle: " << "\n";
	oTriangle.printShapeInfo();
	std::cout << "" << "\n";
	std::cout << "S= " << oTriangle.getArea() << "\n";
	std::cout << "" << "\n";
	oTriangle.move(2, 3);
	oTriangle.printShapeInfo();
	std::cout << "" << "\n";
	oTriangle.move({ 5, 5 });
	oTriangle.printShapeInfo();
	std::cout << "\n";

	return 0;
}