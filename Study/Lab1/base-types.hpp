#ifndef bt_h
#define bt_h


struct point_t {
	double x;
	double y;
};
struct rectangle_t {
	double width;
	double height;
	point_t pos;
};

#endif