#include "shape.hpp"

class Rectangle : public Shape
{
public:
	Rectangle(double width, double height);
	virtual void getArea() const;
};