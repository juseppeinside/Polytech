#ifndef sh_h
#define sh_h
#include <iostream>
#include "base-types.hpp"

class Shape
{
public:
	virtual void getArea() const = 0;
	virtual void move(double dx, double dy, int way);
	rectangle_t Structofrectanglet;
	rectangle_t getFrameRect() const;
};

#endif