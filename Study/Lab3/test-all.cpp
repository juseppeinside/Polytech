#define BOOST_TEST_MODULE test-all

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "compositeshape.hpp"
#include "matrix.hpp"
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>

const double accuracy = 0.01;

BOOST_AUTO_TEST_SUITE(Rectangle_tests)
BOOST_AUTO_TEST_CASE(width_permanenty_after_mone_on)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  const double width = oRectangle.getWidth();
  oRectangle.move(2, 3);
  BOOST_CHECK_CLOSE(oRectangle.getWidth(), width, accuracy);
}
BOOST_AUTO_TEST_CASE(width_permanenty_after_move_to)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  const double width = oRectangle.getWidth();
  oRectangle.move({ 5, 5 });
  BOOST_CHECK_CLOSE(oRectangle.getWidth(), width, accuracy);
}
BOOST_AUTO_TEST_CASE(height_permanenty_after_move_on)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  const double height = oRectangle.getHeight();
  oRectangle.move(2, 3);
  BOOST_CHECK_CLOSE(oRectangle.getHeight(), height, accuracy);
}
BOOST_AUTO_TEST_CASE(height_permanenty_after_move_to)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  const double height = oRectangle.getHeight();
  oRectangle.move({ 5, 5 });
  BOOST_CHECK_CLOSE(oRectangle.getHeight(), height, accuracy);
}
BOOST_AUTO_TEST_CASE(area_permanenty_after_move_on)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  const double area = oRectangle.getArea();
  oRectangle.move(2, 3);
  BOOST_CHECK_CLOSE(oRectangle.getArea(), area, accuracy);
}
BOOST_AUTO_TEST_CASE(area_permanenty_after_move_to)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  const double area = oRectangle.getArea();
  oRectangle.move({ 5, 5 });
  BOOST_CHECK_CLOSE(oRectangle.getArea(), area, accuracy);
}
BOOST_AUTO_TEST_CASE(area_scaling_increase)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  double area = oRectangle.getArea();
  oRectangle.scale(2);
  BOOST_CHECK_CLOSE(oRectangle.getArea(), 4 * area, accuracy);
}
BOOST_AUTO_TEST_CASE(area_scaling_decrease)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  double area = oRectangle.getArea();
  oRectangle.scale(0.5);
  BOOST_CHECK_CLOSE(oRectangle.getArea(), area / 4, accuracy);
}
BOOST_AUTO_TEST_CASE(scale_test)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  BOOST_CHECK_THROW(oRectangle.scale(-99), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(rotate_clockwise)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  const double width = oRectangle.getWidth();
  const double height = oRectangle.getHeight();
  oRectangle.rotate(90);
  BOOST_CHECK_CLOSE(oRectangle.getWidth(), height, accuracy);
  BOOST_CHECK_CLOSE(oRectangle.getHeight(), width, accuracy);
}
BOOST_AUTO_TEST_CASE(rotate_counterclockwise)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  const double width = oRectangle.getWidth();
  const double height = oRectangle.getHeight();
  oRectangle.rotate(90);
  BOOST_CHECK_CLOSE(oRectangle.getWidth(), height, accuracy);
  BOOST_CHECK_CLOSE(oRectangle.getHeight(), width, accuracy);
}
BOOST_AUTO_TEST_CASE(rotate_area)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  const double area = oRectangle.getArea();
  oRectangle.rotate(90);
  BOOST_CHECK_CLOSE(area, oRectangle.getArea(), accuracy);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Circle_tests)
BOOST_AUTO_TEST_CASE(radius_permanenty_after_move_on)
{
  Circle oCircle{ { 1, 1 }, 6 };
  const double radius = oCircle.getRadius();
  oCircle.move(2, 3);
  BOOST_CHECK_CLOSE(oCircle.getRadius(), radius, accuracy);
}
BOOST_AUTO_TEST_CASE(radius_permanenty_after_move_to)
{
  Circle oCircle{ { 1, 1 }, 6 };
  const double radius = oCircle.getRadius();
  oCircle.move({ 5, 5 });
  BOOST_CHECK_CLOSE(oCircle.getRadius(), radius, accuracy);
}
BOOST_AUTO_TEST_CASE(area_permanenty_after_move_on)
{
  Circle oCircle{ { 1, 1 }, 6 };
  const double area = oCircle.getArea();
  oCircle.move(2, 3);
  BOOST_CHECK_CLOSE(oCircle.getArea(), area, accuracy);
}
BOOST_AUTO_TEST_CASE(area_permanenty_after_move_to)
{
  Circle oCircle{ { 1, 1 }, 6 };
  const double area = oCircle.getArea();
  oCircle.move({ 5, 5 });
  BOOST_CHECK_CLOSE(oCircle.getArea(), area, accuracy);
}
BOOST_AUTO_TEST_CASE(area_scaling_increase)
{
  Circle oCircle{ { 1, 1 }, 6 };
  double area = oCircle.getArea();
  oCircle.scale(2);
  BOOST_CHECK_CLOSE(oCircle.getArea(), 4 * area, accuracy);
}
BOOST_AUTO_TEST_CASE(area_scaling_decrease)
{
  Circle oCircle{ { 1, 1 }, 6 };
  double area = oCircle.getArea();
  oCircle.scale(0.5);
  BOOST_CHECK_CLOSE(oCircle.getArea(), area / 4, accuracy);
}
BOOST_AUTO_TEST_CASE(radius_test)
{
  Circle oCircle{ { 1, 1 }, 6 };
  BOOST_CHECK_THROW(oCircle.setRadius(-99), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scale_test)
{
  Circle oCircle{ { 1, 1 }, 6 };
  BOOST_CHECK_THROW(oCircle.scale(-99), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(rotate_clockwise)
{
  Circle oCircle{ { 1, 1 }, 6 };
  rectangle_t frame = oCircle.getFrameRect();
  oCircle.rotate(90);
  BOOST_CHECK(oCircle.getFrameRect() == frame);
}
BOOST_AUTO_TEST_CASE(rotate_counterclockwise)
{
  Circle oCircle{ { 1, 1 }, 6 };
  rectangle_t frame = oCircle.getFrameRect();
  oCircle.rotate(-90);
  BOOST_CHECK(oCircle.getFrameRect() == frame);
}
BOOST_AUTO_TEST_CASE(rotate_area)
{
  Circle oCircle{ { 1, 1 }, 6 };
  const double area = oCircle.getArea();
  oCircle.rotate(90);
  BOOST_CHECK_CLOSE(area, oCircle.getArea(), accuracy);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Triangle_tests)
BOOST_AUTO_TEST_CASE(area_permanenty_after_move_on)
{
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  const double area = oTriangle.getArea();
  oTriangle.move(2, 3);
  BOOST_CHECK_CLOSE(oTriangle.getArea(), area, accuracy);
}
BOOST_AUTO_TEST_CASE(area_permanenty_after_move_to)
{
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  const double area = oTriangle.getArea();
  oTriangle.move({ 5, 5 });
  BOOST_CHECK_CLOSE(oTriangle.getArea(), area, accuracy);
}
BOOST_AUTO_TEST_CASE(area_scaling_increase)
{
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  double area = oTriangle.getArea();
  oTriangle.scale(2);
  BOOST_CHECK_CLOSE(oTriangle.getArea(), 4 * area, accuracy);
}
BOOST_AUTO_TEST_CASE(area_scaling_decrease)
{
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  double area = oTriangle.getArea();
  oTriangle.scale(0.5);
  BOOST_CHECK_CLOSE(oTriangle.getArea(), area / 4, accuracy);
}
BOOST_AUTO_TEST_CASE(points_test)
{
  BOOST_CHECK_THROW(Triangle tri({ 10, 10 }, { 10, 10 }, { 10, 10 }), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scale_test)
{
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  BOOST_CHECK_THROW(oTriangle.scale(-99), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(rotate_clockwise)
{
  Triangle oTriangle({ 3,1 }, { 6,1 }, { 6,7 });
  oTriangle.rotate(90);
  rectangle_t frame = { 6, 3, { 5, 3 } };
  BOOST_CHECK(oTriangle.getFrameRect() == frame);
}
BOOST_AUTO_TEST_CASE(rotate_counterclockwise)
{
  Triangle oTriangle({ 3,1 }, { 6,1 }, { 6,7 });
  oTriangle.rotate(90);
  rectangle_t frame = { 6, 3,{ 5, 3 } };
  BOOST_CHECK(oTriangle.getFrameRect() == frame);
}
BOOST_AUTO_TEST_CASE(rotate_area)
{
  Triangle oTriangle({ 3,1 }, { 6,1 }, { 6,7 });
  const double area = oTriangle.getArea();
  oTriangle.rotate(90);
  BOOST_CHECK_CLOSE(area, oTriangle.getArea(), 0.001);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CopmoSiteShape_test)
BOOST_AUTO_TEST_CASE(not_correct_construcor)
{
  Shape *oShape = nullptr;
  BOOST_CHECK_THROW(CompositeShape oCompositeShape(oShape), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(compositeshape_rectangle)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  CompositeShape oCompositeShape(&oRectangle);
  BOOST_CHECK(&oCompositeShape[0] == &oRectangle);
}
BOOST_AUTO_TEST_CASE(compositeshape_circle)
{
  Circle oCircle{ { 1, 1 }, 6 };
  CompositeShape oCompositeShape(&oCircle);
  BOOST_CHECK(&oCompositeShape[0] == &oCircle);
}
BOOST_AUTO_TEST_CASE(compositeshape_triangle)
{
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  CompositeShape oCompositeShape(&oTriangle);
  BOOST_CHECK(&oCompositeShape[0] == &oTriangle);
}
BOOST_AUTO_TEST_CASE(not_correct_addShape)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  CompositeShape oCompositeShape(&oRectangle);
  Shape *oShape = nullptr;
  BOOST_CHECK_THROW(oCompositeShape.addShape(oShape), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(add_compositeshape_rectangle)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  CompositeShape oCompositeShape(&oRectangle);
  Rectangle ooRectangle{ { 5, 5 }, 5, 5 };
  oCompositeShape.addShape(&ooRectangle);
  BOOST_CHECK(&oCompositeShape[1] == &ooRectangle);
}
BOOST_AUTO_TEST_CASE(area_compositeshape)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  Circle oCircle{ { 1, 1 }, 6 };
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  CompositeShape oCompositeShape(&oRectangle);
  oCompositeShape.addShape(&oCircle);
  oCompositeShape.addShape(&oTriangle);
  BOOST_CHECK_CLOSE(oRectangle.getArea() + oCircle.getArea() + oTriangle.getArea(), oCompositeShape.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(move_to_position_area_compositeshape)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  Circle oCircle{ { 1, 1 }, 6 };
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  CompositeShape oCompositeShape(&oRectangle);
  oCompositeShape.addShape(&oCircle);
  oCompositeShape.addShape(&oTriangle);
  const double past_area = oCompositeShape.getArea();
  oCompositeShape.move({ 5,5 });
  BOOST_CHECK_CLOSE(past_area, oCompositeShape.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(move_by_axis_area_compositeshape)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  Circle oCircle{ { 1, 1 }, 6 };
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  CompositeShape oCompositeShape(&oRectangle);
  oCompositeShape.addShape(&oCircle);
  oCompositeShape.addShape(&oTriangle);
  const double past_area = oCompositeShape.getArea();
  oCompositeShape.move(2, 3);
  BOOST_CHECK_CLOSE(past_area, oCompositeShape.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(area_scaling_increase)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  Circle oCircle{ { 1, 1 }, 6 };
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  CompositeShape oCompositeShape(&oRectangle);
  oCompositeShape.addShape(&oCircle);
  oCompositeShape.addShape(&oTriangle);
  const double past_area = oCompositeShape.getArea();
  oCompositeShape.scale(2);
  BOOST_CHECK_CLOSE(past_area, oCompositeShape.getArea() / 4, accuracy);
}
BOOST_AUTO_TEST_CASE(area_scaling_decrease)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  Circle oCircle{ { 1, 1 }, 6 };
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  CompositeShape oCompositeShape(&oRectangle);
  oCompositeShape.addShape(&oCircle);
  oCompositeShape.addShape(&oTriangle);
  const double past_area = oCompositeShape.getArea();
  oCompositeShape.scale(0.5);
  BOOST_CHECK_CLOSE(past_area, oCompositeShape.getArea() * 4, accuracy);
}
BOOST_AUTO_TEST_CASE(scale_wrong_negative_coefficient)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  Circle oCircle{ { 1, 1 }, 6 };
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  CompositeShape oCompositeShape(&oRectangle);
  oCompositeShape.addShape(&oCircle);
  oCompositeShape.addShape(&oTriangle);
  BOOST_CHECK_THROW(oCompositeShape.scale(-99), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scale_wrong_null_coefficient)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  Circle oCircle{ { 1, 1 }, 6 };
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  CompositeShape oCompositeShape(&oRectangle);
  oCompositeShape.addShape(&oCircle);
  oCompositeShape.addShape(&oTriangle);
  BOOST_CHECK_THROW(oCompositeShape.scale(0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(equality)
{
  Rectangle oRectangle{ { 4, 5 }, 5, 5 };
  Circle oCircle{ { 1, 1 }, 6 };
  Triangle oTriangle{ { 0, 0 },{ 10, 10 },{ 20, 0 } };
  CompositeShape cs_lhs(&oRectangle);
  CompositeShape cs_rhs(&oRectangle);
  cs_lhs.addShape(&oCircle);
  cs_rhs.addShape(&oCircle);
  cs_lhs.addShape(&oTriangle);
  cs_rhs.addShape(&oTriangle);
  BOOST_CHECK(cs_lhs == cs_rhs);
}
BOOST_AUTO_TEST_CASE(rotate_clockwise)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  CompositeShape oCompositeShape(&oRectangle);
  Circle oCircle{ { 1, 1 }, 6 };
  oCompositeShape.rotate(90);
  rectangle_t frame = { 3, 2, {1.5, 1} };
  BOOST_CHECK(oCompositeShape.getFrameRect() == frame);
}
BOOST_AUTO_TEST_CASE(rotate_counterclockwise)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  CompositeShape oCompositeShape(&oRectangle);
  Circle oCircle{ { 1, 1 }, 6 };
  oCompositeShape.rotate(-90);
  rectangle_t frame = { 3, 2,{ 1.5, 1 } };
  BOOST_CHECK(oCompositeShape.getFrameRect() == frame);
}
BOOST_AUTO_TEST_CASE(compositeshape_turn_area)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  CompositeShape oCompositeShape(&oRectangle);
  Circle oCircle{ { 1, 1 }, 6 };
  const double area = oCompositeShape.getArea();
  oCompositeShape.rotate(-90);
  BOOST_CHECK_CLOSE(area, oCompositeShape.getArea(), accuracy);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Matrix_tests)
BOOST_AUTO_TEST_CASE(new_shape)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  Matrix oMatrix(&oRectangle);
  BOOST_CHECK(oMatrix[0][0] == &oRectangle);
}
BOOST_AUTO_TEST_CASE(new_lines)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  Triangle oTriangle({ 3,1 }, { 6,1 }, { 6,7 });
  Matrix oMatrix(&oRectangle);
  oMatrix.addShape(&oTriangle);
  BOOST_CHECK(oMatrix[1][0] == &oTriangle);
}
BOOST_AUTO_TEST_CASE(new_colums)
{
  Rectangle oRectangle{ { 1, 1 }, 2, 3 };
  Circle oCircle{ { 1, 1 }, 6 };
  Triangle oTriangle({ 3,1 }, { 6,1 }, { 6,7 });
  Matrix oMatrix(&oTriangle);
  oMatrix.addShape(&oRectangle);
  oMatrix.addShape(&oCircle);
  BOOST_CHECK(oMatrix[1][1] == &oCircle);
}
BOOST_AUTO_TEST_CASE(new_compositeshape)
{
  Rectangle oRectangle{ { 6, 3 }, 4, 2 };
  Circle oCircle{ { 3, 3 }, 1 };
  CompositeShape oCompositeShape(&oRectangle);
  oCompositeShape.addShape(&oCircle);
  Matrix oMatrix(&oCompositeShape);
  BOOST_CHECK(&oCompositeShape == oMatrix[0][0]);
}
BOOST_AUTO_TEST_SUITE_END()
