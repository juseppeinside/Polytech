//Создать приложение с 6 классами и 3 интерфейсами. Интерфейсы : Игрушка с методом играть, Дом с методом жить, Еда с методом есть .Классы - Мячик, Кукла ;Котлета, Бутерброд;
//Юрта, Коттедж; Каждый класс реализует соответствующий интерфейс. Все интерфейсы унаследованы от интерфейса Предмет с методом цена.
//В приложении создать массив объектов Предмет , состоящий из объектов всех классов (количество элементов задано параметром ).Вывести все Предметы на дисплей.
//Перебирая в цикле элементы массива Предмет, находить  c помощью оператора  instanceof  те объекты, которые реализуют  интерфейс Еда.
//Для каждого найденного элемента массива Предмет, реализующего  интерфейс Еда, выполнить метод интерфейса Еда и метод интерфейса Предмет. Вывести результаты поиска.



/**
 * @author Maxim Shkividorov
 * @version 1.1
 */

public class SecondLab {
    static Subject[] obj= new Subject[6];
    static Food[] objEat = new Food[6];
    public static void main(String[] args) {
        obj[0] = new Ball(Ball.class.getSimpleName());
        obj[1] = new Doll(Doll.class.getSimpleName());
        obj[2] = new Cutlet(Cutlet.class.getSimpleName());
        obj[3] = new Sandwich(Sandwich.class.getSimpleName());
        obj[4] = new Yurt(Yurt.class.getSimpleName());
        obj[5] = new Cottage(Cottage.class.getSimpleName());
        objEat[2] = new Cutlet(Cutlet.class.getSimpleName());
        objEat[3] = new Sandwich(Sandwich.class.getSimpleName());

        for (int i = 0; i < 6; ++i) {
            System.out.println(obj[i]);
        }

        for (int i = 0; i < 5; ++i) {
            if (obj[i] instanceof Food){
                System.out.println(obj[i]);
                obj[i].price();
                objEat[i].eat();
            }
        }

    }
}

interface Toy extends Subject{
    void play();
}

interface Home extends Subject{
    void life();
}

interface Food extends Subject{
    void eat();
}

interface Subject{
    void price();
}

class Ball implements Toy{
    @Override
    public void price() {

    }

    @Override
    public void play() {

    }
    String name;
    Ball(String name) {
        this.name = name;
    }
}

class Doll implements Toy{
    @Override
    public void price() {

    }

    @Override
    public void play() {

    }
    String name;
    Doll(String name) {
        this.name = name;
    }
}

class Cutlet implements Food{
    @Override
    public void price() {
    }

    @Override
    public void eat() {

    }
    String name;
    Cutlet(String name) {
        this.name = name;
    }
}

class Sandwich implements Food{
    @Override
    public void price() {

    }

    @Override
    public void eat() {

    }
    String name;
    Sandwich(String name) {
        this.name = name;
    }
}

class Yurt implements Home {
    @Override
    public void price() {

    }

    @Override
    public void life() {

    }
    String name;
    Yurt(String name) {
        this.name = name;
    }
}

class Cottage implements Home{
    @Override
    public void price() {

    }

    @Override
    public void life() {

    }
    String name;
    Cottage(String name) {
        this.name = name;
    }
}

