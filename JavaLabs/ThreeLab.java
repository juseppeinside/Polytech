/*3. Напишите приложение с 2 потоками- производителем и потребителем, которые имеют разделяемую статическую переменную- склад. То есть склад не является массивом!
Склад хранит целые числа от 0 до 100.
Производитель генерирует число от 0 до 100 и отправляет его на склад, выводя имя производителя и сгенеренное число.
Потребитель читает значение числа со склада, как только оно там появилось, и выводит  на консоль имя потребителя и полученное число.
  Выполнить задание   с использованием конструкции synchronized . 
Не использовать в этом задании флаги для синхронизации потоков, а только методы wait и notify. 
Также не использовать любые задержки для потоков после начала их работы в виде методов sleep, yield или wait c параметром. */

/**
 * @author Maxim Shkividorov
 * @version 1.0
 */


import java.util.Random;

public class ConsumerAndProducer {
    public static void main(String[] args) {
        Stock stock = new Stock();
        Producer producer = new Producer(stock);
        Consumer consumer = new Consumer(stock);
          new Thread(producer).start();
          new Thread(consumer).start();
    }

}

class Producer implements Runnable {
    Stock stock;
    Producer(Stock stock){
        this.stock=stock;
    }

    Random random = new Random(47);

    @Override
    public void run(){
        for (int i = 1; i < 100; i++) {

            stock.put(random.nextInt(100));
        }
    }
}

class Consumer implements Runnable {
    Stock stock;
    Consumer(Stock stock){
        this.stock=stock;
    }
    Random random = new Random(47);

    @Override
    public void run(){
        for (int i = 1; i < 100; i++) {
            stock.get(random.nextInt(100));
        }
    }
}

class Stock {
    private int product=0;

    public synchronized void get(int random) {
        while (product<1) {
            try {
                wait();
            }
            catch (InterruptedException e) {
            }
        }
        product--;
        System.out.println("Consumer bought item " + random );
        notify();
    }

    public synchronized void put(int random) {
        while (product>=1) {
            try {
                wait();
            }
            catch (InterruptedException e) {
            }
        }
        product++;
        System.out.println("Producer added item" + random);
        System.out.println("Products in stock: " + product);
        notify();
    }
}