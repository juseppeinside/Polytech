import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;

public class FirstLabWithPerioidBetween {
    public static void main(String[] args) {
        LocalDate start = LocalDate.of(1998, 4, 22);
        LocalDate end = LocalDate.now(ZoneId.systemDefault());
        Period p = Period.between(start, end);
        System.out.print(p.getYears() + " year" + (p.getYears() > 1 ? "s " : " "));
        System.out.print(p.getMonths() + " month" + (p.getMonths() > 1 ? "s and " : " and "));
        System.out.print(p.getDays() + " day" + (p.getDays() > 1 ? "s.\n" : ".\n"));
    }
}
