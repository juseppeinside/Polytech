#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
  FILE *src, *dst;
  size_t bytes = 0;
  unsigned char buf[BUFSIZ];

  if ((src = fopen("1.txt", "r")) == NULL) {
    fprintf(stderr, "Cannot open source file");
    return EXIT_FAILURE;
  }

  if ((dst = fopen("2.ascii", "w")) == NULL) {
    fprintf(stderr, "Cannot open target file");
    return EXIT_FAILURE;
  }

  while ((bytes = fread(buf, sizeof(unsigned char), BUFSIZ, src)) > 0) {
    if (fwrite(buf, sizeof(unsigned char), bytes, dst) != bytes) {

      fprintf(stderr, "Failure to write");
      return EXIT_FAILURE;
    }
  }

  fclose(src);
  fclose(dst);
  return EXIT_SUCCESS;
}